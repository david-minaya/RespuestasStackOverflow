# Respuestas Stack Overflow en español

En este proyecto se recopilan varias respuestas a preguntas realizadas en [Stack Overflow en español](https://es.stackoverflow.com/).

Cada commit representa la respuesta a una pregunta. A su vez los commit tienen el nombre y la url de la pregunta.

> No todas las respuestas que se encuentran en este proyecto dan solucion a las preguntas.
 
##### Listado de preguntas

- [beforeTextChanged, android Studio y editText numerico](https://gitlab.com/david-minaya/RespuestasStackOverflow/)

- [Evitar dejar el gridview oculto cuando no hay resultados en la busqueda](https://gitlab.com/david-minaya/RespuestasStackOverflow/tree/73d7b740e7d022a959c21b9c9adaf17133b36485)

- [ListView tapa a TextView](https://gitlab.com/david-minaya/RespuestasStackOverflow/tree/f3971d974c887d6b236c4806cb9ba41f7ebeb8cc)

- [Cambiar color barra de notificaciones android studio](https://gitlab.com/david-minaya/RespuestasStackOverflow/tree/ae118d74ec1f099657f101f5622138eacae27f25)

- [controlar visibilidad de un botón](https://gitlab.com/david-minaya/RespuestasStackOverflow/tree/bc8e824a6ec180861188569e823cb0c87c0093f8)

- [Filtro Android Studio , ListView Con adapter Perzonalizado](https://gitlab.com/david-minaya/RespuestasStackOverflow/tree/8a88add20227a6c674797b9c16f41dc2faffff34)

- [¿Cómo lanzar un Fragment heredado?](https://gitlab.com/david-minaya/RespuestasStackOverflow/tree/21aa64219cb04db7bcb3fd933dbf641c8bcc1134)

- [Cómo hacer clickeables 2 o más elementos en una cardview?](https://gitlab.com/david-minaya/RespuestasStackOverflow/tree/fe9a1695a9ebd3749fd4b95e2635bd20df099b7b)

- [Android Studio verificar caracteres de un TextView](https://gitlab.com/david-minaya/RespuestasStackOverflow/tree/fc178be8c60d10729dda2aab8050887cd968a08e)

- [Leer QR desde fragment y retornar el resultado al fragment](https://gitlab.com/david-minaya/RespuestasStackOverflow/tree/429dd6fcad12492704934d202ce4ea72df8e75a0)

- [Almacenar un objeto Json, que contenga subobjetos, usando JSONObject Volley](https://gitlab.com/david-minaya/RespuestasStackOverflow/tree/82dbf7b2ecd0955de5e305c28ed4d2035a1ef3a0)

- [Cambiar color de fondo(background) de lista (AlertDialog) al seleccionar una opción, y que se mantenga al volver a abrir](https://gitlab.com/david-minaya/RespuestasStackOverflow/tree/08481bf7877b7ef4d8750bfb1259566d335874c8)

- [¿Como dividir un elemento de una ListView con hashmap?](https://gitlab.com/david-minaya/RespuestasStackOverflow/tree/d74c171f598c3a2bddcf1c11ea7b7cb0a9efcfe1)