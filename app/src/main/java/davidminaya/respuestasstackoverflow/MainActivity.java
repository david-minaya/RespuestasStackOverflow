package davidminaya.respuestasstackoverflow;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.widget.EditText;
import android.widget.TextView;

/**
 * beforeTextChanged, android Studio y editText numerico
 *
 * https://es.stackoverflow.com/questions/134838/beforetextchanged-android-studio-y-edittext-numerico
 * */

public class MainActivity extends AppCompatActivity {

    TextView texto;
    EditText editar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        texto = (TextView)findViewById(R.id.texto);
        editar = (EditText)findViewById(R.id.editar);

        final int valorPorDefecto = 50;
        texto.setText(""+valorPorDefecto);

        editar.addTextChangedListener(new TextWatcher() {
            @Override
            public void beforeTextChanged(CharSequence s, int start, int count, int after) {

            }

            @Override
            public void onTextChanged(CharSequence s, int start, int before, int count) {

            }

            @Override
            public void afterTextChanged(Editable s) {

                int valor = valorPorDefecto;
                int menos;

                // Si el editText esta vacio o se ingresa un caracter no
                // numerico, a la variable menos se le asigna el valor de cero.
                try {
                    menos = Integer.parseInt(editar.getText().toString());
                } catch (NumberFormatException e) {
                    menos = 0;
                }

                int calcular = valor - menos;
                texto.setText(""+calcular);
            }
        });
    }
}
